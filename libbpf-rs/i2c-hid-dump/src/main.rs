// SPDX-License-Identifier: (LGPL-2.1 OR BSD-2-Clause)

use core::time::Duration;
use std::cmp;
use std::sync::atomic::{AtomicU64, Ordering};

use anyhow::{bail, Result};
use chrono::{Local, Utc, TimeZone};
use libbpf_rs::PerfBufferBuilder;
use plain::Plain;
use structopt::StructOpt;
use phf::{phf_map};

#[path = "bpf/.output/i2chiddump.skel.rs"]
mod i2chiddump;
use i2chiddump::*;

static BOOT_TS: AtomicU64 = AtomicU64::new(0);

/// Trace high run queue latency
#[derive(Debug, StructOpt)]
struct Command {
    // /// Trace latency higher than this value
    // #[structopt(default_value = "10000")]
    // latency: u64,

    /// Verbose debug output
    #[structopt(short, long)]
    verbose: bool,
}

#[repr(C)]
#[derive(Default)]
struct Event {
    pub ts: u64,
    pub client_addr: u16,
    pub hid_descr_reg: u16,
    pub req_type: u16,
    pub report_id: u16,
    pub report_len: u16,
    pub padding: u16,
    pub data0: [u8; 32],
    pub data1: [u8; 32],
}

unsafe impl Plain for Event {}

static REQ_TYPES: phf::Map<u16, &str> = phf_map! {
    0x01u16 => "GET_REPORT",
    0x02u16 => "GET_IDLE",
    0x03u16 => "GET_PROTOCOL",
    0x09u16 => "SET_REPORT",
    0x0au16 => "SET_IDLE",
    0x0bu16 => "SET_PROTOCOL",
    0xf0u16 => "ALLOC_BUFFER",
    0xf1u16 => "IRQ_EVENT",
};

fn bump_memlock_rlimit() -> Result<()> {
    let rlimit = libc::rlimit {
        rlim_cur: 128 << 20,
        rlim_max: 128 << 20,
    };

    if unsafe { libc::setrlimit(libc::RLIMIT_MEMLOCK, &rlimit) } != 0 {
        bail!("Failed to increase rlimit");
    }

    Ok(())
}

fn print_data(data0: &[u8], data1: &[u8], len: u16) -> String {
    if len == 0 {
        return " -".to_string();
    }

    let i0 : usize = cmp::min(len.into(), 32);
    let mut str_data0 : String = data0[0..i0].iter().map(|x| format!(" {:02x}", x)).collect();

    if len >= 32 {
        let i1 : usize = cmp::min((len - 32).into(), 32);
        let str_data1 : String = data1[0..i1].iter().map(|x| format!(" {:02x}", x)).collect();

        str_data0.push_str(&str_data1);
    }

    if len >= 64 {
        str_data0.push_str(" ...");
    }

    return str_data0;
}

fn get_ts(kts: u64) -> u64 {
    if BOOT_TS.load(Ordering::SeqCst) == 0 {
        let now = Local::now().timestamp_nanos() as u64;
        BOOT_TS.store(now - kts, Ordering::SeqCst);
    }

    return BOOT_TS.load(Ordering::SeqCst) + kts;
}

fn handle_event(_cpu: i32, data: &[u8]) {
    let mut event = Event::default();
    plain::copy_from_bytes(&mut event, data).expect("Data buffer was too short");

    match REQ_TYPES.get(&event.req_type) {
        Some(req) => println!("{:>35}    0x{:02x}       0x{:02x} {:>16}        0x{:02x} {:>4}  {}",
                              Utc.timestamp((get_ts(event.ts) / 1000000000) as i64,
                                            (get_ts(event.ts) % 1000000000) as u32).to_rfc3339(),
                              event.client_addr,
                              event.hid_descr_reg,
                              req,
                              event.report_id,
                              event.report_len,
                              print_data(&event.data0, &event.data1, event.report_len)),
        None => println!("unknown request type: 0x{:0x} .", event.req_type)
    }
}

fn handle_lost_events(cpu: i32, count: u64) {
    eprintln!("Lost {} events on CPU {}", count, cpu);
}

fn main() -> Result<()> {
    let opts = Command::from_args();

    let mut skel_builder = I2chiddumpSkelBuilder::default();
    if opts.verbose {
        skel_builder.obj_builder.debug(true);
    }

    bump_memlock_rlimit()?;
    let mut open_skel = skel_builder.open()?;

    // Write arguments into prog
    //open_skel.rodata().min_us = opts.latency;
    //open_skel.rodata().targ_pid = opts.pid;
    //open_skel.rodata().targ_tgid = opts.tid;

    // Begin tracing
    let mut skel = open_skel.load()?;
    skel.attach()?;
    println!("Tracing... Hit Ctrl-C to end.");
    println!("{:>35} {:>7} {:>10} {:>16} {:>11} {:>4}   {}", "TIME", "CLIENT", "HID_DESCR", "REQUEST", "REPORT_ID", "LEN", "DATA");

    let perf = PerfBufferBuilder::new(skel.maps_mut().events())
        .sample_cb(handle_event)
        .lost_cb(handle_lost_events)
        .build()?;

    loop {
        perf.poll(Duration::from_millis(100))?;
    }
}
