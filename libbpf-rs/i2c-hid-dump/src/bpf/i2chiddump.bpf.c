// SPDX-License-Identifier: GPL-2.0
// Copyright (c) 2020 Benjamin Tissoires
#include "vmlinux.h"
#include <bpf/bpf_helpers.h>
#include <bpf/bpf_core_read.h>
#include <bpf/bpf_tracing.h>
#include "i2chiddump.h"

#define HID_MIN_BUFFER_SIZE 64
#define PT_REGS_PARM6(ctx)	((ctx)->r9)

#define min(a,b) ((a) < (b) ? (a) : (b))
#define I2C_HID_READ_PENDING (1 << 2)

struct i2c_hid_desc {
	__le16 wHIDDescLength;
	__le16 bcdVersion;
	__le16 wReportDescLength;
	__le16 wReportDescRegister;
	__le16 wInputRegister;
	__le16 wMaxInputLength;
	__le16 wOutputRegister;
	__le16 wMaxOutputLength;
	__le16 wCommandRegister;
	__le16 wDataRegister;
	__le16 wVendorID;
	__le16 wProductID;
	__le16 wVersionID;
	__le32 reserved;
} __attribute__((packed));

struct i2c_hid {
	struct i2c_client	*client;	/* i2c client */
	struct hid_device	*hid;	/* pointer to corresponding HID dev */
	union {
		__u8 hdesc_buffer[sizeof(struct i2c_hid_desc)];
		struct i2c_hid_desc hdesc;	/* the HID Descriptor */
	};
	__le16			wHIDDescRegister; /* location of the i2c
						   * register of the HID
						   * descriptor. */
	unsigned int		bufsize;	/* i2c buffer size */
	u8			*inbuf;		/* Input buffer */
	u8			*rawbuf;	/* Raw Input buffer */
	u8			*cmdbuf;	/* Command buffer */
	u8			*argsbuf;	/* Command arguments buffer */

	unsigned long		flags;		/* device flags */
	unsigned long		quirks;		/* Various quirks */

        /* truncated */
//	wait_queue_head_t	wait;		/* For waiting the interrupt */
//
//	struct i2c_hid_platform_data pdata;
//
//	bool			irq_wake_enabled;
//	struct mutex		reset_lock;
};

struct {
	__uint(type, BPF_MAP_TYPE_ARRAY);
	__uint(max_entries, 1);
	__type(key, u32);
	__type(value, u64);
} parameters SEC(".maps");

struct {
	__uint(type, BPF_MAP_TYPE_PERF_EVENT_ARRAY);
	__uint(key_size, sizeof(u32));
	__uint(value_size, sizeof(u32));
} events SEC(".maps");

struct ihid {
	u16 hid_descr_addr;
	u16 i2c_addr;
	u16 max_input_length;
};

struct {
    __uint(type, BPF_MAP_TYPE_HASH);
    __uint(max_entries, 4096);
    __type(key, struct hid_device *);
    __type(value, struct ihid);
} ihid_map SEC(".maps");

struct stack_args {
	struct i2c_hid i2c_hid;
	struct i2c_client i2c_client;
	struct hid_device hid;
};

struct {
	__uint(type, BPF_MAP_TYPE_PERCPU_ARRAY);
	__uint(max_entries, 1);
	__type(key, u32);
	__type(value, struct stack_args);
} tmp_storage_map SEC(".maps");

/*
 * the following is a tad complex because some probes
 * have access to struct hid_device, when others only have
 * struct i2c_hid.
 *
 * Given that HID events are receiving the struct hid_device
 * version, we store that in the hashmap as a key, and
 * leave the others deal with more expansive operations
 */
static int fetch_hid_info(struct hid_device *hid_ptr,
			  struct i2c_hid *ihid_ptr,
			  struct event *event)
{
	struct ihid ihid_info_value = {0};
	struct ihid *ihid_info = 0;

	if (hid_ptr)
		ihid_info = bpf_map_lookup_elem(&ihid_map, &hid_ptr);

	if (!ihid_info) {
		u32 map_id = 0;
		struct stack_args *stack_args_value;

		stack_args_value = bpf_map_lookup_elem(&tmp_storage_map, &map_id);
		if (!stack_args_value)
			return -1;

		if (hid_ptr && !ihid_ptr) {
			/* retrieve the struct i2c_hid field from struct hid_device) */
			bpf_probe_read(&stack_args_value->hid, sizeof(struct hid_device),
					(void *)(hid_ptr));

			bpf_probe_read(&stack_args_value->i2c_client, sizeof(struct i2c_client),
					(void *)(stack_args_value->hid.driver_data));

			ihid_ptr = stack_args_value->i2c_client.dev.driver_data;
		}

		if (ihid_ptr) {
			/* Retrieve the info from the kernel space */
			bpf_probe_read(&stack_args_value->i2c_hid, sizeof(struct i2c_hid),
				       (void *)ihid_ptr);

			hid_ptr = stack_args_value->i2c_hid.hid;

			bpf_probe_read(&stack_args_value->i2c_client, sizeof(struct i2c_client),
					(void *)(stack_args_value->i2c_hid.client));

			/* store the info in the hashmap */

			ihid_info_value.i2c_addr = stack_args_value->i2c_client.addr;
			ihid_info_value.hid_descr_addr = stack_args_value->i2c_hid.wHIDDescRegister;
			ihid_info_value.max_input_length = stack_args_value->i2c_hid.hdesc.wMaxInputLength;

		} else {
			return -1;
		}

		ihid_info = &ihid_info_value;

		if (hid_ptr) {
			bpf_map_update_elem(&ihid_map, &hid_ptr, ihid_info, 0 /*flags */);
		}

	}

	if (ihid_info) {
		event->hid_descr_reg = ihid_info->hid_descr_addr;
		event->client_addr = ihid_info->i2c_addr;
		event->report_len = ihid_info->max_input_length;
	}

	return 0;
}

SEC("kprobe/i2c_hid_alloc_buffers")
int BPF_KPROBE(kprobe__i2c_hid_alloc_buffers,
		struct i2c_hid *ihid_ptr,
		size_t report_size)
{
	struct event __data = {0};

	if (report_size != HID_MIN_BUFFER_SIZE)
		return 0;

	__data.ts = bpf_ktime_get_boot_ns();

	__data.req_type = (__u16)HID_ALLOC_BUFFER;

	if (fetch_hid_info(0, ihid_ptr, &__data))
		return 0;

	bpf_perf_event_output(ctx, &events, BPF_F_CURRENT_CPU,
			&__data, sizeof(__data));
	return 0;
}

struct raw_request {
	struct hid_device *hid;
	struct i2c_hid *ihid;
	u64 ts;
	__u16 req_type;
	__u16 report_id;
	__u16 report_len;
	u8 *buf;
};

struct {
    __uint(type, BPF_MAP_TYPE_HASH);
    __uint(max_entries, 4096);
    __type(key, u64);
    __type(value, struct raw_request);
} raw_request_map SEC(".maps");

SEC("kprobe/i2c_hid_raw_request")
int BPF_KPROBE(kprobe__i2c_hid_raw_request,
		struct hid_device *hid,
		unsigned char reportnum,
		__u8 *buf,
		size_t len,
		unsigned char rtype)
		//int reqtype)
{
	struct raw_request __data = {0};
	int reqtype = PT_REGS_PARM6(ctx);
	u64 task;

	__data.ts = bpf_ktime_get_boot_ns();
	__data.hid = hid;
	__data.report_id = reportnum;
	__data.report_len = (__u16)len;
	__data.req_type = (__u16)reqtype;
	__data.buf = buf;

	task = bpf_get_current_task();

	bpf_map_update_elem(&raw_request_map, &task, &__data, 0 /* flags */);

	return 0;
}

SEC("kretprobe/i2c_hid_raw_request")
int BPF_KRETPROBE(kretprobe__i2c_hid_raw_request)
{
	struct event __data = {0};
	u64 task = bpf_get_current_task();
	struct raw_request *query_ptr;

	query_ptr = bpf_map_lookup_elem(&raw_request_map, &task);
	if (!query_ptr) {
		// bpf_trace_printk("perf_output failed, no lookup\\n");
		goto cleanup;
	}

	if (fetch_hid_info(query_ptr->hid, 0, &__data))
		return 0;

	__data.ts = query_ptr->ts;
	__data.report_id = query_ptr->report_id;
	__data.req_type = query_ptr->req_type;
	__data.report_len = query_ptr->report_len;

	bpf_probe_read(&__data.data, min(sizeof(__data.data), query_ptr->report_len),
			(void *)(query_ptr->buf));

	bpf_perf_event_output(ctx, &events, BPF_F_CURRENT_CPU,
			&__data, sizeof(__data));

cleanup:
	bpf_map_delete_elem(&raw_request_map, &task);
	return 0;
}

SEC("kprobe/i2c_hid_irq")
int BPF_KPROBE(kprobe__i2c_hid_irq, int irq, void *dev_id)
{
	struct raw_request __data = {0};
	u64 task;;

	__data.ts = bpf_ktime_get_boot_ns();
	__data.ihid = (struct i2c_hid *)dev_id;

	task = bpf_get_current_task();
	bpf_map_update_elem(&raw_request_map, &task, &__data, 0 /* flags */);

	return 0;
}

SEC("kretprobe/i2c_hid_irq")
int BPF_KRETPROBE(kretprobe__i2c_hid_irq)
{
	struct event __data = {0};
	u64 task = bpf_get_current_task();
	struct raw_request *query_ptr;
	struct stack_args *stack_args_value;
	u32 map_id = 0;

	query_ptr = bpf_map_lookup_elem(&raw_request_map, &task);
	if (!query_ptr) {
		// bpf_trace_printk("perf_output failed, no lookup\\n");
		goto cleanup;
	}

	stack_args_value = bpf_map_lookup_elem(&tmp_storage_map, &map_id);
	if (!stack_args_value)
		goto cleanup;

	/* Retrieve the info from the kernel space */
	bpf_probe_read(&stack_args_value->i2c_hid, sizeof(struct i2c_hid),
		       query_ptr->ihid);

	if (stack_args_value->i2c_hid.flags & I2C_HID_READ_PENDING)
		goto cleanup;

	if (fetch_hid_info(stack_args_value->i2c_hid.hid, 0, &__data))
		goto cleanup;

	__data.ts = query_ptr->ts;
	__data.req_type = 0xf1;

	bpf_probe_read(&__data.data, min(sizeof(__data.data), __data.report_len),
			(void *)(stack_args_value->i2c_hid.inbuf));

	__data.report_len = __data.data[0] | (__data.data[1] << 8);
	__data.report_id = __data.data[2];

	bpf_perf_event_output(ctx, &events, BPF_F_CURRENT_CPU,
			&__data, sizeof(__data));

cleanup:
	bpf_map_delete_elem(&raw_request_map, &task);
	return 0;
}

char LICENSE[] SEC("license") = "GPL";
