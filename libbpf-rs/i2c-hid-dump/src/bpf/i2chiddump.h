#ifndef __I2CHIDDUMP_H
#define __I2CHIDDUMP_H

#define DATA_LEN	64

enum HID_REQUESTS {
	HID_REQ_GET_REPORT = 0x01,
	HID_REQ_GET_IDLE = 0x2,
	HID_REQ_GET_PROTOCOL = 0x3,
	HID_REQ_SET_REPORT = 0x9,
	HID_REQ_SET_IDLE = 0xa,
	HID_REQ_SET_PROTOCOL = 0xb,
	HID_ALLOC_BUFFER = 0xf0,
};

struct event {
	__u64 ts;
	unsigned short client_addr;
	__u16 hid_descr_reg;
	__u16 req_type;
	__u16 report_id;
	__u16 report_len;
	__u16 padding;
	__u8 data[DATA_LEN];
};

#endif /* __I2CHIDDUMP_H_ */
