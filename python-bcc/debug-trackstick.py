#!/usr/bin/python
#
# This is a Hello World example that uses BPF_PERF_OUTPUT.

from bcc import BPF

MAX_HID_DATA = 30

# define BPF program
prog = f"""
#include <linux/ptrace.h>
#include <linux/hid.h>

#define MAX_HID_DATA {MAX_HID_DATA}""" + """


struct probe_data_t
{
        s32 v[MAX_HID_DATA];
};

BPF_PERF_OUTPUT(probe_function_events);


int probe_function(struct pt_regs *ctx)
{
        struct hid_device *hid;
        struct hid_field *field;
        struct hid_usage *usage;
        __s32 value;
        u8 i;

        struct probe_data_t __data = {0};


        hid = (struct hid_device *)PT_REGS_PARM1(ctx);
        field = (struct hid_field *)PT_REGS_PARM2(ctx);
        usage = (struct hid_usage *)PT_REGS_PARM3(ctx);
        value = (__s32)PT_REGS_PARM4(ctx);

//       /* does not work: read only memory section */
//       report[1] = 3;

        __data.v[0] = (int) usage->type;
        __data.v[1] = (int) usage->code;
        __data.v[2] = value;
        __data.v[3] = (int) field->hidinput;
        if (field->hidinput)
            __data.v[4] = (int) field->hidinput->input;


        probe_function_events.perf_submit(ctx, &__data, sizeof(__data));
        return 0;
}
"""

# load BPF program
b = BPF(text=prog)
b.attach_kprobe(event="hidinput_hid_event", fn_name="probe_function")

# process event
def print_event(cpu, data, size):
    event = b["probe_function_events"].event(data)

    # guess work to find the length of the report
    for end in range(MAX_HID_DATA, 1, -1):
        if event.v[end - 1]:
            break

    stream = f"event: {end}"
    for i in range(end):
        stream += f" {event.v[i]:02x}"
    print(stream)

# loop with callback to print_event
b["probe_function_events"].open_perf_buffer(print_event)
while True:
    try:
        b.perf_buffer_poll()
    except KeyboardInterrupt:
        exit()
