#!/usr/bin/python
#
# This is a Hello World example that uses BPF_PERF_OUTPUT.

from bcc import BPF

MAX_HID_DATA = 30

HID_REQUESTS = {
    0x1: "HID_REQ_GET_REPORT",
    0x2: "HID_REQ_GET_IDLE",
    0x3: "HID_REQ_GET_PROTOCOL",
    0x9: "HID_REQ_SET_REPORT",
    0xa: "HID_REQ_SET_IDLE",
    0xb: "HID_REQ_SET_PROTOCOL",
}

# define BPF program
prog = f"""
#include <linux/ptrace.h>
#include <linux/hid.h>

#define MAX_HID_DATA {MAX_HID_DATA}""" + """

#define PT_REGS_PARM6(ctx)	((ctx)->r9)
#define PT_REGS_PARM7(ctx)	((ctx)->sp)

struct probe_data_t
{
        u8 reportID;
        int len;
        int reqtype;
        u8 v[MAX_HID_DATA];
};

BPF_PERF_OUTPUT(probe_function_events);

BPF_HASH(queries, u64, struct probe_data_t);
BPF_HASH(bufs, u64, u8 *);

int kprobe__i2c_hid_raw_request(struct pt_regs *ctx,
                                struct hid_device *hid,
                                unsigned char reportnum,
                                __u8 *buf,
                                size_t len,
                                unsigned char rtype,
                                int reqtype)
{
        int i, rc;

        struct probe_data_t __data = {0};
        u64 zero = 0;

        __data.reportID = reportnum;
        __data.len = (int)len;
        __data.reqtype = reqtype;
        for (i=0; i < len && i < MAX_HID_DATA; i++) {
            __data.v[i] = (u8)buf[i];
        }

        queries.update(&zero, &__data);
        bufs.update(&zero, &buf);

//        rc = probe_function_events.perf_submit(ctx, &__data, sizeof(__data));
//        if (rc < 0)
//            bpf_trace_printk("perf_output failed: %d\\n", rc);
        return 0;
}

int kretprobe__i2c_hid_raw_request(struct pt_regs *ctx)
{
    u64 zero = 0;
    struct probe_data_t *query_ptr = queries.lookup(&zero);
    __u8 **bufp = bufs.lookup(&zero);
    int i, rc;

    if (!query_ptr || !bufp) {
        bpf_trace_printk("perf_output failed, no lookup\\n");
        return 0;
    }

    __u8 *buf = *bufp;

    for (i=0; i < query_ptr->len && i < MAX_HID_DATA; i++) {
        query_ptr->v[i] = buf[i];
    }

    rc = probe_function_events.perf_submit(ctx, query_ptr, sizeof(struct probe_data_t));
    if (rc < 0)
        bpf_trace_printk("perf_output failed: %d\\n", rc);

    return 0;
}
"""

# load BPF program
b = BPF(text=prog)

# process event
def print_event(cpu, data, size):
    event = b["probe_function_events"].event(data)

    stream = f"{HID_REQUESTS[event.reqtype]} -> {event.reportID:>2} (0x{event.reportID:02x}) len: {event.len:>5}: "
    for i in range(min(event.len, MAX_HID_DATA)):
        stream += f" {event.v[i]:02x}"
    if event.len > MAX_HID_DATA:
        stream += " ..."
    print(stream)

# loop with callback to print_event
b["probe_function_events"].open_perf_buffer(print_event)
print("Tracing... Hit Ctrl-C to end.")
while True:
    try:
        b.perf_buffer_poll()
    except KeyboardInterrupt:
        exit()
