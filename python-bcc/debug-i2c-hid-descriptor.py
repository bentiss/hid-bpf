#!/usr/bin/python
#
# This is a Hello World example that uses BPF_PERF_OUTPUT.

from bcc import BPF

# define BPF program
prog = """
#include <linux/ptrace.h>
#include <linux/hid.h>
#include <linux/i2c.h>

struct i2c_hid_desc {
	__le16 wHIDDescLength;
	__le16 bcdVersion;
	__le16 wReportDescLength;
	__le16 wReportDescRegister;
	__le16 wInputRegister;
	__le16 wMaxInputLength;
	__le16 wOutputRegister;
	__le16 wMaxOutputLength;
	__le16 wCommandRegister;
	__le16 wDataRegister;
	__le16 wVendorID;
	__le16 wProductID;
	__le16 wVersionID;
	__le32 reserved;
} __packed;

struct i2c_hid {
	struct i2c_client	*client;	/* i2c client */
	struct hid_device	*hid;	/* pointer to corresponding HID dev */
	union {
		__u8 hdesc_buffer[sizeof(struct i2c_hid_desc)];
		struct i2c_hid_desc hdesc;	/* the HID Descriptor */
	};
	__le16			wHIDDescRegister; /* location of the i2c
						   * register of the HID
						   * descriptor. */
	unsigned int		bufsize;	/* i2c buffer size */
	u8			*inbuf;		/* Input buffer */
	u8			*rawbuf;	/* Raw Input buffer */
	u8			*cmdbuf;	/* Command buffer */
	u8			*argsbuf;	/* Command arguments buffer */

	unsigned long		flags;		/* device flags */
	unsigned long		quirks;		/* Various quirks */

        /* truncated */
//	wait_queue_head_t	wait;		/* For waiting the interrupt */
//
//	struct i2c_hid_platform_data pdata;
//
//	bool			irq_wake_enabled;
//	struct mutex		reset_lock;
};

struct probe_data_t
{
        u16 i2c_address;
        u16 hid_descriptor_address;
};

BPF_PERF_OUTPUT(probe_function_events);

int kprobe__i2c_hid_alloc_buffers(struct pt_regs *ctx,
                                  struct i2c_hid *ihid,
                                  size_t report_size)
{
        int rc;

        struct probe_data_t __data = {0};

        if (report_size != HID_MIN_BUFFER_SIZE)
            return 0;

        __data.i2c_address = ihid->client->addr;
        __data.hid_descriptor_address = ihid->wHIDDescRegister;

        rc = probe_function_events.perf_submit(ctx, &__data, sizeof(__data));
//        if (rc < 0)
//            bpf_trace_printk("perf_output failed: %d\\n", rc);
        return 0;
}
"""

# load BPF program
b = BPF(text=prog)

# process event
def print_event(cpu, data, size):
    event = b["probe_function_events"].event(data)

    stream = f"-> i2c at 0x{event.i2c_address:x} has HID descriptor at 0x{event.hid_descriptor_address:x}"
    print(stream)

# loop with callback to print_event
b["probe_function_events"].open_perf_buffer(print_event)
print("Tracing... Hit Ctrl-C to end.")
while True:
    try:
        b.perf_buffer_poll()
    except KeyboardInterrupt:
        exit()
