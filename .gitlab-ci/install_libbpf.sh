set -e
set -x

git clone -b v0.1.0 https://github.com/libbpf/libbpf.git
cd libbpf/src/
make
make install
